# Are Web Components A Thing Yet?

Yes.

## Contributing

1. Alphabetize
1. Small images
    1. ...in SVG format

### License

This code is licensed under the MIT license. However, the logos, names, and/or wordmarks of the various companies, organizations, and institutions are copyright and/or trademark their respective owners. The license in this project does not supercede any ownership claims of the entities listed. Those images and/or text is included in this project as non-profit, educational content, not as either endorsement of this project, or for any monetary gain.
